package clase1.clase1Ej3;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 * Created by tritoon on 10/08/16.
 */
public class Main3 {

    private static final int N = 100000;
    private static final int INCREMENTO = 10;


    public static void main(String[] args) throws InterruptedException {
        CuentaBancaria3 cb = new CuentaBancaria3();

        ExecutorService executor = Executors.newCachedThreadPool();

        for(int i=0; i< N; i++){
            executor.submit(()-> cb.depositar(INCREMENTO));
        }

        // Libera los recursos del executorService para el resto de la ejecución
        executor.shutdown();
        // Siempre que espero lo hago por tiempo
        executor.awaitTermination(10, TimeUnit.SECONDS);

        System.out.println(cb.leerMonto());
    }

}
