package clase1.clase1Ej3;

/**
 * Created by tritoon on 10/08/16.
 */
public class CuentaBancaria3 {

    private static final int montoInicial = 1000;
    private long monto = montoInicial;

    public long leerMonto()
    {
        return monto;
    }

    public void depositar(long valor)
    {
        monto += valor;
    }
}
