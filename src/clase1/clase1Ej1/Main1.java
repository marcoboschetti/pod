package clase1.clase1Ej1;

/**
 * Created by tritoon on 10/08/16.
 */
public class Main1 {

    private static final int N = 100000;
    private static final int INCREMENTO = 10;


    public static void main(String[] args) {
        CuentaBancaria1 cb = new CuentaBancaria1();
        for(int i=0; i < N; i++){
            cb.depositar(INCREMENTO);
        }
        System.out.println(cb.leerMonto());
    }

}
