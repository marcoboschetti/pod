package clase1.clase1Ej4;

/**
 * Created by tritoon on 10/08/16.
 */
public class CuentaBancaria4 {

    private static final int montoInicial = 1000;
    private long monto = montoInicial;

    synchronized public long leerMonto()
    {
        return monto;
    }

    synchronized public void depositar(long valor)
    {
        monto += valor;
    }
}
