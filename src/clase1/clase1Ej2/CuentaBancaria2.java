package clase1.clase1Ej2;

/**
 * Created by tritoon on 10/08/16.
 */
public class CuentaBancaria2 {

    private static final int montoInicial = 1000;
    private long monto = montoInicial;

    public long leerMonto()
    {
        return monto;
    }

    public void depositar(long valor)
    {
        monto += valor;
    }
}
