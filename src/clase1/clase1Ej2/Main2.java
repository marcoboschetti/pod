package clase1.clase1Ej2;

/**
 * Created by tritoon on 10/08/16.
 */
public class Main2 {

    private static final int N = 100000;
    private static final int INCREMENTO = 10;


    public static void main(String[] args) throws InterruptedException {
        CuentaBancaria2 cb = new CuentaBancaria2();

        int threads = N;
        Thread[] pool = new Thread[threads];

        for(int i=0; i< threads; i++){
            pool[i] = new Thread(){
                public void run(){
                    cb.depositar(INCREMENTO);
                }
            };
            pool[i].start();
        }

        for(int i=0; i< threads; i++){
            pool[i].join();
        }

        System.out.println(cb.leerMonto());
    }

}
