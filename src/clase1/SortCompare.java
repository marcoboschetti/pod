package clase1;

import java.util.Arrays;
import java.util.Random;

/**
 * Created by tritoon on 10/08/16.
 */
public class SortCompare {

    public static void main(String[] args) {
        int tries = 3;
        int[] sizes = {10000000,25000000,50000000};

        for(int size: sizes){
            int parallelAvg=0, nonParAvg=0;
            for(int i=0;i < tries; i++){
                long[] ans = generateTest(size);
                nonParAvg += ans[0]/tries;
                parallelAvg += ans[1]/tries;
            }
            System.out.println(String.format("Size: %d - Parallel(%d),NonParallel(%d)",size, parallelAvg, nonParAvg ));
        }
    }

    private static long[] generateTest(int size){
        int[] veca, vecb;
        veca = generateRandomArray(size);
        vecb = veca.clone();

        long parallel;

        long start = System.currentTimeMillis();
        Arrays.parallelSort(veca);
        parallel = System.currentTimeMillis() - start;

        start = System.currentTimeMillis();
        Arrays.parallelSort(vecb);
        return new long[]{System.currentTimeMillis() - start,parallel};
    }

    private static int[] generateRandomArray(int size){
        Random rnd = new Random();
        int []vec= new int[size];

        for (int i = 0; i < size; i++)
        {
            int rand= rnd.nextInt();
            vec[i] = rand;
        }
        return vec;
    }
}
