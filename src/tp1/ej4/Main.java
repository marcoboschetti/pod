package tp1.ej4;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 * Created by tritoon on 17/08/16.
 */
public class Main {
    private static Stack stack;

    public static void main(String[] args) throws InterruptedException {


        ExecutorService pool = Executors.newFixedThreadPool(3);

        stack = new Stack();


        for (int i = 0; i < 600; i++) {
            pool.execute(() -> {
                while (true) {
                    try {
                        stack.push(3);
                        stack.pop();
                    } catch (IllegalStateException e) {
                        System.out.print("Boom ");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

            });
        }

        pool.shutdown();
        pool.awaitTermination(1, TimeUnit.SECONDS);
    }
}
