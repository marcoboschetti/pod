package tp1.ej6;

import java.io.File;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 * Created by tritoon on 17/08/16.
 */
public class FileLinesCounter {

    private static Integer COUNT = 0;

    public static void main(String[] args) throws Exception {

        final File folder = new File("/etc");

        ExecutorService pool = Executors.newFixedThreadPool(10);

        for (final File fileEntry : folder.listFiles()) {
            System.out.println("Starting to count: "+fileEntry);
            pool.submit(new FileCounter(fileEntry));
        }

        pool.shutdown();
        while(!pool.awaitTermination(1, TimeUnit.SECONDS));

        System.out.println("Count: "+COUNT);

    }


    public static void increaseLinesCount(int lines) {
        synchronized (COUNT){
            COUNT += lines;
        }
    }
}
