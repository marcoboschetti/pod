package tp1.ej6;

import sun.applet.Main;

import java.io.File;
import java.io.FileReader;
import java.io.LineNumberReader;
import java.util.concurrent.Callable;

/**
 * Created by tritoon on 17/08/16.
 */
public class FileCounter implements Callable {

    private File file;

    public FileCounter(File file) {
        this.file = file;
    }

    @Override
    public Integer call() throws Exception {
        System.out.println("Counting: "+this.file.getName());
        LineNumberReader lnr = new LineNumberReader(new FileReader(file));
        lnr.skip(Long.MAX_VALUE);
        int lines = lnr.getLineNumber(); //Add 1 because line index starts at 0
        lnr.close();

        FileLinesCounter.increaseLinesCount(lines);
        System.out.println("The file: "+this.file.getName()+" has "+lines+" lines");

        return null;
    }
}
