package tp1.ej2;

import tp1.ej1.ConcurrentThreads;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by tritoon on 17/08/16.
 */
public class Main {

    public static void main(String[] args) {
        final ExecutorService pool = Executors.newFixedThreadPool(3);
        pool.execute(new Thread(){
            @Override
            public void run() {
                A.method();
            }
        });
        pool.execute(()-> B.method());
        pool.execute(new Thread(){
            @Override
            public void run() {
                C.method();
            }
        });
        pool.shutdown();
    }

}
