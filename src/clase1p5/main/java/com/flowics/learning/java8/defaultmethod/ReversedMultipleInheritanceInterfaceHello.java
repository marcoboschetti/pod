/**
 * Copyright (c) 2011-2015 Zauber S.A. -- All rights reserved
 */
package clase1p5.main.java.com.flowics.learning.java8.defaultmethod;

/**
 * One of 2 classes (the other {@link MultipleInheritanceInterfaceHello}) to test inheritance with to
 * interfaces (dependant) that define the same default method.
 *
 * @author Marcelo
 * @since Jul 30, 2015
 */
public class ReversedMultipleInheritanceInterfaceHello implements OverridingHelloInterface, BaseHelloInterfaz {

}
